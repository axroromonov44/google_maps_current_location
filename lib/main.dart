import 'package:flutter/material.dart';
import 'package:google_maps_current_location/home_screen.dart';
import 'package:google_maps_current_location/screens/current_location.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Google Maps',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const CurrentLocation(),
    );
  }
}