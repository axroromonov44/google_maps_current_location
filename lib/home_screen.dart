import 'package:flutter/material.dart';
import 'package:google_maps_current_location/screens/current_location.dart';
import 'package:google_maps_current_location/screens/simple_map.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Google Maps'),
        centerTitle: true,
      ),
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const SimpleMap(),
                  ),
                );
              },
              child: const Text(
                'Simple Google Maps',
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CurrentLocation(),
                  ),
                );
              },
              child: const Text(
                'Current Location',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
